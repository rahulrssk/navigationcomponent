package com.rhl.bottommenuexample.fragment.dashbord

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rhl.bottommenuexample.R

class DashbordFragment : Fragment() {

    companion object {
        private val TAG = DashbordFragment::class.java.name
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashbord, container, false)
    }
}